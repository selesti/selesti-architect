<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8">
		<title>Base Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" type="text/css" href="assets/vendor/vendor.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/app.css">
	</head>
	<body>
		<h1>Selesti Architect</h1>

        <script type="text/javascript" src="assets/vendor/vendor.min.js"></script>
        <script type="text/javascript" src="assets/js/app.min.js"></script>

        <script async="" src="//localhost:3000/browser-sync/browser-sync-client.2.12.5.js"></script>
    </body>
</html>
