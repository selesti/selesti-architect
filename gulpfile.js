//Utilities
var os             = require('os');
var exec           = require('child_process').exec;
var request        = require('request');
var fs             = require('fs');
var gulp           = require('gulp');
var browserSync    = require('browser-sync');
var reload         = browserSync.reload;
var runSequence    = require('run-sequence');
var pngquant       = require('imagemin-pngquant');
var plumber        = require('gulp-plumber');
var rename         = require('gulp-rename');
var notify         = require("gulp-notify");
var sourcemaps     = require('gulp-sourcemaps');
var mainBowerFiles = require('main-bower-files');
var clean          = require('gulp-clean');
var concat         = require('gulp-concat');
var gulpif         = require('gulp-if');
var newer          = require('gulp-newer');
var cached         = require('gulp-cached');
var env            = require('gulp-env');

//Theming
var scsslint       = require('gulp-scss-lint');
var sass           = require('gulp-sass');
var autoprefixer   = require('gulp-autoprefixer');
var cssnano        = require('gulp-cssnano');
var coffeelint     = require('gulp-coffeelint');
var coffee         = require('gulp-coffee');
var babel          = require('gulp-babel');
var eslint         = require('gulp-eslint');
var uglify         = require('gulp-uglify');
var imagemin       = require('gulp-imagemin');
var phplint        = require('gulp-phplint');
var phpcs          = require('gulp-phpcs');

//Configurations
var pkg            = require('./package.json');
var vhost          = 'machine.' + process.cwd().split('/').pop() + '.dev';
var themedir       = pkg.themeAssets.outputFolder;
var srcdir         = pkg.themeAssets.inputFolder;
var appdir         = pkg.themeAssets.appFolder;
var iconid         = pkg.themeAssets.iconIdent;
var icondst        = pkg.themeAssets.iconDest;
var humanstxt      = pkg.themeAssets.humanstxt;

//Parsing custom args
var refresh_php    = true;
var refresh_js     = true;

for(i = 0; i < process.argv.length; i++){

    var arg = process.argv[i].replace(/-/g, '').toLowerCase();

    switch (arg) {
        case 'nophp':
        case 'np':
            refresh_php = false;
            console.log('Skipping BrowserSync on PHP files...');
        break;
        case 'nojs':
        case 'nj':
            refresh_js = false;
            console.log('Skipping BrowserSync on JS files...');
        break;
    }
}

//Error Handling
var plumberErrorHandler = {
    'errorHandler' : notify.onError({
        title: 'Gulp',
        message: 'Error: <%= error.message %>'
    })
};

//Vendor Tasks
gulp.task('bower', function(){

    exec('bower prune', function(err, out){
        console.log(out);
    });

    return gulp.src(mainBowerFiles())
    .pipe(plumber(plumberErrorHandler))
    .pipe(sourcemaps.init())
    .pipe(gulpif('*.js', concat('vendor.js')))
    .pipe(gulpif('*.css', concat('vendor.css')))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(themedir + 'vendor'))
    .pipe(browserSync.stream());
});

gulp.task('bower-clean', function(){
    return gulp.src([themedir + 'vendor/vendor.*'])
    .pipe(clean());
});

gulp.task('bower-concat', function(){
    return gulp.src(themedir + 'vendor/vendor.*')
    .pipe(gulpif('*.js', concat('vendor.min.js')))
    .pipe(gulpif('*.css', concat('vendor.min.css')))
    .pipe(gulp.dest(themedir + 'vendor'));
});

gulp.task('bower-minify', ['bower'], function(){
    return gulp.src(themedir + 'vendor/vendor.min.*')
    .pipe(gulpif('*.js', uglify()))
    .pipe(gulpif('*.css', cssnano()))
    .pipe(gulp.dest(themedir + 'vendor'));
});

//Scss Tasks
gulp.task('scss-lint', function () {
    return gulp.src(srcdir + 'scss/**/*.scss')
    .pipe(cached('scss-lint'))
    .pipe(scsslint({'config':'.scssrc.yml'}));
});

gulp.task('scss-compile', ['scss-lint'], function(){
    return gulp.src(srcdir + 'scss/**/*.scss')
    .pipe(plumber(plumberErrorHandler))
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(themedir + 'css'))
    .pipe(browserSync.stream());
});

gulp.task('scss-compile-prefix-minify', ['scss-lint'], function(){

    env.set({
        BROWSERSLIST_CONFIG: './.browserslist'
    });

    return gulp.src(srcdir + 'scss/**/*.scss')
    .pipe(plumber(plumberErrorHandler))
    .pipe(sass())
    .pipe(autoprefixer({
        remove: false
    }))
    .pipe(cssnano())
    .pipe(rename({
        suffix: '.min'
    }))
    .pipe(gulp.dest(themedir + 'css'))
    .pipe(browserSync.stream());
});

//Js Tasks
gulp.task('js-lint', function(){
    return gulp.src([
        srcdir + 'javascript/**/*.js',
        '!' + srcdir + 'javascript/vendor/**/*.js'
    ])
    .pipe(plumber(plumberErrorHandler))
    .pipe(cached('js-lint'))
    .pipe(eslint({configFile: '.eslintrc.yml'}))
    .pipe(eslint.format())
    /*.pipe(eslint.result(function (result) {
            // Called for each
            console.log('ESLint result: ' + result.filePath);
            console.log('# Messages: ' + result.messages.length);
            console.log('# Warnings: ' + result.warningCount);
            console.log('# Errors: ' + result.errorCount);
    }))*/
    .pipe(eslint.failOnError());
});

gulp.task('coffee-lint', function(){
    return gulp.src([
        srcdir + 'coffee/vendor/*/**.coffee',
        srcdir + 'coffee/app.coffee',
        srcdir + 'coffee/**/*.coffee'
    ])
    .pipe(coffeelint())
    .pipe(coffeelint.reporter());
})

gulp.task('js-concat', ['js-lint', 'coffee-lint'], function(){
    return gulp.src([
        srcdir + 'javascript/vendor/*/**.js',
        srcdir + 'javascript/app.js',
        srcdir + 'javascript/**/*.js',
        srcdir + 'coffee/vendor/*/**.coffee',
        srcdir + 'coffee/app.coffee',
        srcdir + 'coffee/**/*.coffee',
        srcdir + 'es6/vendor/*/**.es6',
        srcdir + 'es6/app.es6',
        srcdir + 'es6/**/*.es6'
    ])
    .pipe(plumber(plumberErrorHandler))
    .pipe(sourcemaps.init())
    .pipe(gulpif('*.coffee', coffee({bare: true})))
    .pipe(gulpif('*.es6', babel({presets: ['es2015']})))
    .pipe(concat('app.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(themedir + 'js'));
});

gulp.task('js-minify', ['js-lint', 'coffee-lint'], function(){
    return gulp.src([
        srcdir + 'javascript/vendor/*/**.js',
        srcdir + 'javascript/app.js',
        srcdir + 'javascript/**/*.js',
        srcdir + 'coffee/vendor/*/**.coffee',
        srcdir + 'coffee/app.coffee',
        srcdir + 'coffee/**/*.coffee',
        srcdir + 'es6/vendor/*/**.es6',
        srcdir + 'es6/app.es6',
        srcdir + 'es6/**/*.es6'
    ])
    .pipe(plumber(plumberErrorHandler))
    .pipe(gulpif('*.coffee', coffee({bare: true})))
    .pipe(gulpif('*.es6', babel({presets: ['es2015']})))
    .pipe(uglify())
    .pipe(concat('app.js'))
    .pipe(rename({
        suffix: '.min'
    }))
    .pipe(gulp.dest(themedir + 'js'))
    .pipe(browserSync.stream());
});

//Image Tasks
gulp.task('newer-imagemin', function(){
    console.log('minifying newer images');
    return gulp.src( srcdir + '/images/**/*.{gif,svg,png,jpg}')
    .pipe(newer(themedir + 'img'))
    .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        use: [pngquant()]
    }))
    .pipe(gulp.dest( themedir + 'img'));
});

gulp.task('imagemin', function(){
    console.log('minifying all images');
    return gulp.src( srcdir + '/images/**/*.{gif,svg,png,jpg}')
    .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        use: [pngquant()]
    }))
    .pipe(gulp.dest( themedir + 'img'));
});

gulp.task('image-extras', function(){
    return gulp.src( srcdir + 'images/**/*.{xml,ico,json}')
    .pipe(gulp.dest( themedir + 'img'));
});

gulp.task('videos', function(){
    console.log('Consider using vimeo');
    return gulp.src( srcdir + 'videos/**/*.{mp4,mov,ogv,webm}')
    .pipe(gulp.dest( themedir + 'video'));
})

gulp.task('clean-images', function(){
    return gulp.src( themedir + 'img', {read: false} )
    .pipe(clean());
});

//Font Tasks
gulp.task('fonts', function(){
    return gulp.src( srcdir + 'fonts/**/*.{woff,woff2,ttf,otf,eot}')
    .pipe(gulp.dest( themedir + 'fonts'));
});

//Humans.txt Download
gulp.task('humans', function(){

    var api_url = 'https://api.bitbucket.org/2.0/snippets/' + humanstxt;

    return request.get( api_url , function( err, resp, body){

        //make sure we have the remote file
        if( err ) {
            return console.error(err);
        }

        //try find the file
        try {
            var file_url = JSON.parse(body).files['humans.txt'].links.self.href;

            return request.get( file_url, function( _err, _resp, _body){
                console.log('Updating httpdocs/humans.txt with ' + file_url);
                console.log(_body);
            })
            .on('error', function(error){
                console.error('Failed to fetch ' + file_url);
            })
            .pipe(fs.createWriteStream('httpdocs/humans.txt'));
        }
        catch(e){
            console.error(e);
        }
    });
});

//Icon Tasks
gulp.task('icomoon-svgx', function(){

    var matches,
        definitions,
        svgx = 'https://i.icomoon.io/public/' + iconid + '/svgxuse.js';

    return request.get( svgx, function(err, resp, body){

        //make sure we have the remote file
        if( err ) {
            return console.error(err);
        }

        //we now need to find the url for the definitions
        matches = body.match(/fallback =(.*)\'/im);

        //check the url looks about right
        if( matches.length == 2 && typeof matches[1] === 'string'){

            definitions = matches[1].replace(/\'/igm, '');

            //fetch the remote definition file
            //and write it to our template
            return request
            .get(definitions, function(){
                console.log('Updated ' + icondst);
            })
            .on('error', function(error){
                console.error(error)
            })
            .pipe(fs.createWriteStream( icondst ));
        }
        else {
            console.error('Couldnt find svg definitions, time to check the regex :(');
        }
    });
});

gulp.task('icomoon', function(){

    var definitions = 'https://i.icomoon.io/public/' + iconid + '/symbol-defs.svg?' + Date.now();

    return request
    .get(definitions, function(){
        console.log('Updated ' + icondst);
    })
    .on('error', function(error){
        console.error(error)
    })
    .pipe(fs.createWriteStream( icondst ));
})

//PHP Tasks
gulp.task('php-lint', function(){
    return gulp.src([appdir + '**/*.php', '!' + appdir + 'logs/**/*.php', '!' + appdir + 'libraries/**/*.php'])
    .pipe(cached('php-lint'))
    .pipe(phplint())
    .pipe(phplint.reporter('fail'));
});

gulp.task('php-psr', function(){
    return gulp.src([appdir + '**/*.php', '!' + appdir + 'logs/**/*.php', '!' + appdir + 'libraries/**/*.php', '!' + appdir + 'views/**/*.php'])
    .pipe(cached('php-psr'))
    .pipe(phpcs({
        bin: os.homedir() + '/.composer/vendor/bin/phpcs',
        standard: 'PSR2',
        warningSeverity: 0
    }))
    .pipe(phpcs.reporter('log'));
});

//Frontend Admin Tasks
gulp.task('admin', function(){
    return gulp.src(srcdir + 'bower_components/selesti-admin/dist/**/*.min.{css,js}')
    .pipe(gulp.dest( themedir + 'vendor/selesti'));
});

//Server Tasks
gulp.task('serve', [], function(){

    browserSync({
        notify: false,
        port: 3000,
        open: false,
        proxy: vhost
    });

    //recompile/lint css and js
    gulp.watch(srcdir + 'scss/**/*.scss', ['scss-compile']);
    gulp.watch([
        srcdir + 'coffee/**/*.coffee',
        srcdir + 'es6/**/*.es6',
        srcdir + 'javascript/**/*.js'
    ], ['js-concat']);

    gulp.watch([
        appdir + '**/*.php',
        '!' + appdir + 'logs/**.*'
    ], ['php-lint']);

    gulp.watch('./bower.json', ['bower']);

    gulp.watch(themedir + 'img/**/*').on('change', browserSync.stream);
    gulp.watch(srcdir + 'images/**/*', ['newer-imagemin']);

    var refresh_on = [];

    if( refresh_js ){
        refresh_on.push(srcdir + 'javascript/**/*.js');
    }

    if( refresh_php ){
        refresh_on.push(appdir + '**/*.php', '!' + appdir + 'logs/*.php');
    }

    gulp.watch(refresh_on).on('change', browserSync.reload);
});

//Task Aliases
gulp.task('default', function(cb){
    runSequence(['js-minify', 'scss-compile-prefix-minify', 'clean-images', 'fonts', 'bower-clean'], 'bower', 'bower-concat', ['bower-minify', 'imagemin', 'image-extras', 'admin'] );
});

gulp.task('dev', function(cb){
    runSequence('scss-compile', 'js-concat', 'bower', 'newer-imagemin');
});

gulp.task('lint', function(cb){
    runSequence('js-lint', 'scss-lint', 'php-lint', 'php-psr');
});
