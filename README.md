# Selesti Architect #

## Technologies used ##

* [SASS](http://sass-lang.com/L) CSS preprocessor
* [LESS](http://lesscss.org/) CSS preprocessor
* [Bower](https://bower.io/) front-end dependency manager
* [NPM](https://www.npmjs.com/) dependency manager
* [Gulp.js](http://gulpjs.com/) streaming build system
* [BrowserSync](https://www.browsersync.io/)


## Setting up Architect ##

### Do you prefer to use LESS over Sass? ###
Instead of cloning **master**, please clone the **less** branch which has all the LESS dependencies

To set up whichever branch you have chosen:


* Clone repo down to your empty project folder
* Open Terminal, and cd to the project folder 
```
#!bash

cd ~/YOUR_PROJECT_PATH
```

* Install all the node.js dependencies by running
```
#!bash

npm install
```

* Install all the Bower dependencies by running
```
#!bash

bower install
```

* To start developing, launch the BrowserSync server and set gulp to watch for any changes and build your stuff with:
```
#!bash

gulp serve
```

* If you don't have a vhost setup, you can point your browser at the local server, usually **http://localhost:3000** (whatever comes up after *gulp watch* is done; see below) and start developing!

* To utilise BrowserSync's debugging tools, point another tab at the UI URL, which is usually one port above the access: e.g. **http://localhost:3001**

![Screen Shot 2016-08-17 at 16.10.34.png](https://bitbucket.org/repo/LGReRn/images/421884310-Screen%20Shot%202016-08-17%20at%2016.10.34.png)


## Developing in Architect ##

* All resources and assets (LESS/SCSS/JS, images, fonts etc) will be in **theme-assets**, and Gulp will do the necessary leg-work and put them in their respective places inside **httpdocs** which should be considered a generated *dist* version

* If you create new imagery, place them inside **theme-assets/images/**; if you have *gulp serve* running it should automatically minify them and copy them to *httpdocs*, else you can run **gulp newer-imagemin** to manually trigger the process

* If you create new LESS/SCSS/JS files whilst **gulp serve** is running, it might not pick them up and thus might not trigger minification when editing them: in which case just restart the *gulp serve* process, and everything should kick off as expected

* Each time you save a SCSS, LESS or JS file, it should be linted and any issues encountered will be output to the terminal window. However you can manually trigger the linting process by running one of the following, depending on what you are attempting to lint:
```
#!bash

gulp scss-lint
```
```
#!bash

gulp less-lint
```
```
#!bash

gulp js-lint
```
which is useful if you have finished a section of development, and want to make any required modifications to satiate the linting standards.

* To change the front-end dependencies used in the project (Angular, Bootstrap etc), modify **bower.json** and amend the "dependencies" section; you will then need to run **bower install** in your terminal to download the new dependencies, followed by **gulp** to rebuild the vendor asset files.